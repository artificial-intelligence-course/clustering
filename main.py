import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from yellowbrick.cluster import KElbowVisualizer

import warnings

# remove warnings
pd.set_option('mode.chained_assignment', None)


def warn(*args, **kwargs):
    pass


warnings.warn = warn

set_path = 'data.csv'

pd.options.display.float_format = '{:,.2f}'.format
df = pd.read_csv(set_path)

pd.set_option('display.max_columns', None)
pd.set_option('expand_frame_repr', False)

print("\nOG : ", df)

# drop names
labels = df[['patientai']].copy()
print("\nNew\n", labels)
df = df.drop('patientai', axis='columns')

features = list(df.columns)[:-2]
print("\nFeatures:\n", features)

data = df[features]
print("\nData:\n", data)

model = KMeans()
visualizer = KElbowVisualizer(model, k=(2, 15), metric='silhouette', timings=True)
visualizer.fit(df)
visualizer.show()


clustering_kmeans = KMeans(n_clusters=3, precompute_distances="auto", n_jobs=-1)
data['clusters'] = clustering_kmeans.fit_predict(data)
print("\nAfter:\n", data)

new = labels
new = new.join(data)
print("\nAdded labels back:\n", new)

pca_num_components = 2

reduced_data = PCA(n_components=pca_num_components).fit_transform(data)
results = pd.DataFrame(reduced_data, columns=['pca1', 'pca2'])
print("\nResults:\n", results)

sns.scatterplot(x="pca1", y="pca2", hue=data['clusters'], data=results)
plt.title('Clustering with 2 dimensions')
plt.show()

target_row = new.loc[new['patientai'] == 'Vardas1']
target_cluster = target_row['clusters']

print("\nTarget cluster of Vardas1:\n", target_cluster.to_string(index=False))

sick_rows = new.loc[new['clusters'] == int(target_cluster)]

list_of_sick = sick_rows['patientai'].tolist()
print("\nList of infected patients:\n", list_of_sick)
