**Plot:**

![alt text](https://gitlab.com/artificial-intelligence-course/clustering/-/raw/master/clusters.jpg)


**Optimal number of clusters:**

![alt text](https://gitlab.com/artificial-intelligence-course/clustering/-/raw/master/optimal.jpg)
